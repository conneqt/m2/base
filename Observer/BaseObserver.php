<?php
/**
 * Created by PhpStorm.
 * User: jeroen
 * Date: 16-10-17
 * Time: 20:02
 */

namespace Conneqt\Base\Observer;

class BaseObserver
{
    protected $_transactionHelper;

    public function __construct(\Conneqt\Base\Helper\TransactionHelper $transactionHelper)
    {
        $this->_transactionHelper = $transactionHelper;
    }
}