<?php
/**
 * Created by PhpStorm.
 * User: jeroen
 * Date: 16-10-17
 * Time: 20:18
 */

namespace Conneqt\Base\Api\Data;

interface TransactionInterface
{
    const TRANSACTION_ID = 'transaction_id';
    const ENTITY_TYPE = 'entity_type';
    const ENTITY_IDENTIFIER = 'entity_identifier';
    const PULLED_AT = 'pulled_at';

    /**
     * Get Transaction ID
     *
     * @return int
     */
    public function getTransactionId();

    /**
     * Set Entity Type
     *
     * @param string $entityType
     * @return $this
     */
    public function setEntityType($entityType);

    /**
     * Get Entity Type
     *
     * @return string
     */
    public function getEntityType();

    /**
     * Set Entity Identifier
     *
     * @param int $entityIdentifier
     * @return $this
     */
    public function setEntityIdentifier($entityIdentifier);

    /**
     * Get Entity Identifier
     *
     * @return int
     */
    public function getEntityIdentifier();

    /**
     * Set Pulled At Date
     *
     * @param string $pulledAt
     * @return $this
     */
    public function setPulledAt($pulledAt);

    /**
     * Get Pulled At Date
     *
     * @return string
     */
    public function getPulledAt();
}