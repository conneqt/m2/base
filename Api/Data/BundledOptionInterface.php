<?php

namespace Conneqt\Base\Api\Data;

interface BundledOptionInterface
{
    /**
     * @param $qty int
     * @return \Conneqt\Base\Api\Data\BundledOptionSelectionInterface
     */
    public function setQty($qty);

    /**
     * @return int
     */
    public function getQty();

    /**
     * @return \Conneqt\Base\Api\Data\BundledOptionSelectionInterface[]
     */
    public function getSelections();

    /**
     * @param $selections \Conneqt\Base\Api\Data\BundledOptionSelectionInterface[]
     * @return \Conneqt\Base\Api\Data\BundledOptionInterface
     */
    public function setSelections($selections);
}