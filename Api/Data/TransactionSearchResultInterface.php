<?php
/**
 * Created by PhpStorm.
 * User: jeroen
 * Date: 16-10-17
 * Time: 21:45
 */

namespace Conneqt\Base\Api\Data;

interface TransactionSearchResultInterface
{
    /**
     * @return \Conneqt\Base\Api\Data\TransactionInterface[]
     */
    public function getItems();

    /**
     * @param \Conneqt\Base\Api\Data\TransactionInterface[] $items
     * @return void
     */
    public function setItems(array $items);
}