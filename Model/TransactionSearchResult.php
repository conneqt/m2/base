<?php
/**
 * Created by PhpStorm.
 * User: jeroen
 * Date: 16-10-17
 * Time: 21:50
 */

namespace Conneqt\Base\Model;

use Magento\Framework\Api\SearchCriteriaInterface;

class TransactionSearchResult extends \Magento\Framework\Api\SearchResults implements \Conneqt\Base\Api\Data\TransactionSearchResultInterface
{

}