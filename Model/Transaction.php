<?php
/**
 * Created by PhpStorm.
 * User: jeroen
 * Date: 16-10-17
 * Time: 20:18
 */

namespace Conneqt\Base\Model;

/**
 * @method \Conneqt\Base\Model\ResourceModel\Transaction getResource()
 * @method \Conneqt\Base\Model\ResourceModel\Transaction\Collection getCollection()
 */
class Transaction extends \Magento\Framework\Model\AbstractModel implements \Conneqt\Base\Api\Data\TransactionInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'conneqt_base_transaction';
    protected $_cacheTag = 'conneqt_base_transaction';
    protected $_eventPrefix = 'conneqt_base_transaction';

    protected function _construct()
    {
        $this->_init('Conneqt\Base\Model\ResourceModel\Transaction');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Set Entity Type
     *
     * @param string $entityType
     * @return $this
     */
    public function setEntityType($entityType)
    {
        $this->setData(self::ENTITY_TYPE, $entityType);

        return $this;
    }

    /**
     * Get Entity Type
     *
     * @return string
     */
    public function getEntityType()
    {
        return $this->getData(self::ENTITY_TYPE);
    }

    /**
     * Set Entity Identifier
     *
     * @param int $entityIdentifier
     * @return $this
     */
    public function setEntityIdentifier($entityIdentifier)
    {
        $this->setData(self::ENTITY_IDENTIFIER, $entityIdentifier);

        return $this;
    }

    /**
     * Get Entity Identifier
     *
     * @return int
     */
    public function getEntityIdentifier()
    {
        return $this->getData(self::ENTITY_IDENTIFIER);
    }

    /**
     * Get Transaction ID
     *
     * @return int
     */
    public function getTransactionId()
    {
        return $this->getData(self::TRANSACTION_ID);
    }

    /**
     * Set Pulled At Date
     *
     * @param string $pulledAt
     * @return $this
     */
    public function setPulledAt($pulledAt)
    {
        $this->setData(self::PULLED_AT, $pulledAt);

        return $this;
    }

    /**
     * Get Pulled At Date
     *
     * @return string
     */
    public function getPulledAt()
    {
        return $this->getData(self::PULLED_AT);
    }
}