<?php
/**
 * Created by PhpStorm.
 * User: jeroen
 * Date: 16-10-17
 * Time: 20:18
 */

namespace Conneqt\Base\Model\ResourceModel\Transaction;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'transaction_id';


    protected function _construct()
    {
        $this->_init('Conneqt\Base\Model\Transaction', 'Conneqt\Base\Model\ResourceModel\Transaction');
    }

}