<?php
/**
 * Created by PhpStorm.
 * User: jeroen
 * Date: 16-10-17
 * Time: 21:50
 */

namespace Conneqt\Base\Model;

use Magento\Framework\Api\SearchCriteriaInterface;

class TransactionRepository implements \Conneqt\Base\Api\TransactionRepositoryInterface
{
    protected $_transactionFactory;
    protected $_transactionResourceFactory;
    protected $_transactionCollectionFactory;
    protected $_transactionSearchResultFactory;

    public function __construct(
        \Conneqt\Base\Model\TransactionFactory $transactionFactory,
        \Conneqt\Base\Model\ResourceModel\TransactionFactory $transactionResourceFactory,
        \Conneqt\Base\Model\ResourceModel\Transaction\CollectionFactory $transactionCollectionFactory,
        \Conneqt\Base\Api\Data\TransactionSearchResultInterfaceFactory $transactionSearchResultInterfaceFactory
    )
    {
        $this->_transactionFactory = $transactionFactory;
        $this->_transactionResourceFactory = $transactionResourceFactory;
        $this->_transactionCollectionFactory = $transactionCollectionFactory;
        $this->_transactionSearchResultFactory = $transactionSearchResultInterfaceFactory;
    }

    /**
     * @param bool $includePulled
     * @param bool $updatePulled
     * @return \Conneqt\Base\Api\Data\TransactionSearchResultInterface
     */
    public function getList($includePulled, $updatePulled)
    {
        $collection = $this->_transactionCollectionFactory->create();

        if (!$includePulled) {
            $collection->addFieldToFilter('pulled_at', ['null' => true]);
        }

        $collection->load();

        $result = $this->buildSearchResult($collection);

        if ($updatePulled) {
            $collection->setDataToAll('pulled_at', time())->save();
        }

        return $result;
    }

    private function buildSearchResult(\Conneqt\Base\Model\ResourceModel\Transaction\Collection $collection)
    {
        $searchResults = $this->_transactionSearchResultFactory->create();

        $searchResults->setItems($collection->getItems());

        return $searchResults;
    }

    /**
     * @param int $transactionId
     * @return \Conneqt\Base\Api\Data\TransactionInterface
     */
    public function setPulled($transactionId)
    {
        $transaction = $this->_transactionFactory->create();
        $transactionResource = $this->_transactionResourceFactory->create();
        $transactionResource->load($transaction, $transactionId);

        $transaction->setPulledAt(time());
        $transactionResource->save($transaction);

        return $transaction;
    }
}