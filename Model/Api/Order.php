<?php
/**
 * Created by PhpStorm.
 * User: jeroen
 * Date: 12-12-17
 * Time: 13:35
 */

namespace Conneqt\Base\Model\Api;

class Order implements \Conneqt\Base\Api\OrderInterface
{
    protected $cartManagement;
    protected $cartItemRepository;
    protected $cartItemFactory;
    protected $shippingInformationManagement;
    protected $shippingInformationFactory;
    protected $addressFactory;
    protected $paymentInformationManagement;
    protected $paymentInterfaceFactory;
    protected $orderRepository;

    /** @var \Magento\Sales\Api\OrderManagementInterface */
    protected $orderManagement;

    /** @var \Magento\Sales\Api\OrderItemRepositoryInterface */
    protected $orderItemRepository;

    /** @var \Magento\Bundle\Api\ProductOptionRepositoryInterface */
    protected $productOptionRepository;

    /** @var \Magento\Quote\Api\Data\ProductOptionInterfaceFactory */
    protected $productOptionInterfaceFactory;

    /** @var \Magento\Quote\Api\Data\ProductOptionExtensionInterfaceFactory */
    protected $productOptionExtensionInterfaceFactory;

    /** @var \Magento\Bundle\Api\Data\BundleOptionInterfaceFactory */
    protected $bundleOptionInterfaceFactory;

    /** @var \Magento\Sales\Model\Order\Email\Sender\OrderSender */
    protected $orderSender;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    private $addressRepository;

    public function __construct(
        \Magento\Quote\Api\CartManagementInterface $cartManagement,
        \Magento\Quote\Api\CartItemRepositoryInterface $cartItemRepository,
        \Magento\Quote\Api\Data\CartItemInterfaceFactory $cartItemFactory,
        \Magento\Checkout\Api\ShippingInformationManagementInterface $shippingInformationManagement,
        \Magento\Checkout\Api\Data\ShippingInformationInterfaceFactory $shippingInformationFactory,
        \Magento\Quote\Api\Data\AddressInterfaceFactory $addressFactory,
        \Magento\Checkout\Api\PaymentInformationManagementInterface $paymentInformationManagement,
        \Magento\Quote\Api\Data\PaymentInterfaceFactory $paymentInterfaceFactory,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Api\OrderItemRepositoryInterface $orderItemRepository,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
        \Magento\Sales\Api\OrderManagementInterface $orderManagement,
        \Magento\Bundle\Api\ProductOptionRepositoryInterface $productOptionRepository,
        \Magento\Quote\Api\Data\ProductOptionInterfaceFactory $productOptionInterfaceFactory,
        \Magento\Quote\Api\Data\ProductOptionExtensionInterfaceFactory $productOptionExtensionInterfaceFactory,
        \Magento\Bundle\Api\Data\BundleOptionInterfaceFactory $bundleOptionInterfaceFactory,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender
    ) {
        $this->cartManagement = $cartManagement;
        $this->cartItemRepository = $cartItemRepository;
        $this->cartItemFactory = $cartItemFactory;
        $this->shippingInformationManagement = $shippingInformationManagement;
        $this->shippingInformationFactory = $shippingInformationFactory;
        $this->addressFactory = $addressFactory;
        $this->paymentInformationManagement = $paymentInformationManagement;
        $this->paymentInterfaceFactory = $paymentInterfaceFactory;
        $this->orderRepository = $orderRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->customerRepository = $customerRepository;
        $this->addressRepository = $addressRepository;
        $this->orderManagement = $orderManagement;
        $this->productOptionRepository = $productOptionRepository;
        $this->productOptionInterfaceFactory = $productOptionInterfaceFactory;
        $this->productOptionExtensionInterfaceFactory = $productOptionExtensionInterfaceFactory;
        $this->bundleOptionInterfaceFactory = $bundleOptionInterfaceFactory;
        $this->orderSender = $orderSender;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return \Magento\Sales\Api\Data\OrderInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function placeOrder($order)
    {
        $cartId = $this->cartManagement->createEmptyCartForCustomer($order->getCustomerId());

        $cartItems = $this->cartItemRepository->getList($cartId);
        foreach ($cartItems as $cartItem) {
            $this->cartItemRepository->deleteById($cartId, $cartItem->getItemId());
        }

        $savedItems = [];
        /** @var \Magento\Sales\Api\Data\OrderItemInterface $item */
        foreach ($order->getItems() as $item) {
            /** @var \Magento\Quote\Api\Data\CartItemInterface $cartItem */
            $cartItem = $this->cartItemFactory->create();

            $cartItem->setData([
                'quote_id' => $cartId,
                'sku' => $item->getSku(),
                'qty' => $item->getQtyOrdered()
            ]);

            /**
             * Bundled Options
             */
            $extensionAttributes = $item->getExtensionAttributes();
            if ($extensionAttributes !== null && $extensionAttributes->getConneqtBundledOptions() !== null) {
                $productOptionData = [];
                $productOptions = $this->productOptionRepository->getList($item->getSku());

                if (count($productOptions) !== count ($extensionAttributes->getConneqtBundledOptions())) {
                    throw new \Magento\Framework\Webapi\Exception(new \Magento\Framework\Phrase(__($item->getSku() . ' - Product Option count does not match')));
                }

                $index = 0;
                foreach ($extensionAttributes->getConneqtBundledOptions() as $conneqtBundledOption) {
                    $bundleOptionInterface = $this->bundleOptionInterfaceFactory->create();

                    $bundleOptionInterface->setOptionId((int)$productOptions[$index]->getOptionId());
                    $bundleOptionInterface->setOptionQty((int)$conneqtBundledOption->getQty());
                    $bundleOptionInterface->setOptionSelections(array_map(function ($connectBundledOptionSelection) use ($productOptions, $index) {
                        /** @var \Conneqt\Base\Api\Data\BundledOptionSelectionInterface $connectBundledOptionSelection */
                        $selectionId = -1;
                        foreach ($productOptions[$index]->getProductLinks() as $productLink) {
                            if ($productLink->getSku() == $connectBundledOptionSelection->getSku()) {
                                $selectionId = (int)$productLink->getId();
                            }
                        }
                        return $selectionId;
                    }, $conneqtBundledOption->getSelections()));

                    $productOptionData[] = $bundleOptionInterface;
                    $index++;
                }

                $productOption = $this->productOptionInterfaceFactory->create();
                $productOptionExtensionAttributes = $this->productOptionExtensionInterfaceFactory->create();
                $productOptionExtensionAttributes->setBundleOptions($productOptionData);
                $productOption->setExtensionAttributes($productOptionExtensionAttributes);
                $cartItem->setProductOption($productOption);
            }

            try {
                $cartItem = $this->cartItemRepository->save($cartItem);
                $savedItems[] = ['cartItem' => $cartItem, 'orderItem' => $item];
            } catch (\Exception $ex) {
                throw new \Magento\Framework\Webapi\Exception(new \Magento\Framework\Phrase(__($item->getSku() . ' - ' . $ex->getMessage())), $ex->getCode());
            }
        }

        /** @var \Magento\Checkout\Api\Data\ShippingInformationInterface $shippingInformation */
        $shippingInformation = $this->shippingInformationFactory->create();

        if ($order->getBillingAddress() !== null) {
            $billingAddress = $this->addressFactory->create();
            $billingAddress->setData([
                'firstname' => $order->getBillingAddress()->getFirstname(),
                'middlename' => $order->getBillingAddress()->getMiddlename(),
                'lastname' => $order->getBillingAddress()->getLastname(),
                'street' => $order->getBillingAddress()->getStreet(),
                'postcode' => $order->getBillingAddress()->getPostcode(),
                'city' => $order->getBillingAddress()->getCity(),
                'country_id' => $order->getBillingAddress()->getCountryId(),
                'telephone' => $order->getBillingAddress()->getTelephone()
            ]);

            $shippingInformation->setBillingAddress($billingAddress);
        } else {
            $customer = $this->customerRepository->getById($order->getCustomerId());
            $defaultBillingAddress = $this->addressRepository->getById($customer->getDefaultBilling());
            $billingAddress = $this->addressFactory->create();
            $billingAddress->setData([
                'firstname' => $defaultBillingAddress->getFirstname(),
                'middlename' => $defaultBillingAddress->getMiddlename(),
                'lastname' => $defaultBillingAddress->getLastname(),
                'street' => $defaultBillingAddress->getStreet(),
                'postcode' => $defaultBillingAddress->getPostcode(),
                'city' => $defaultBillingAddress->getCity(),
                'country_id' => $defaultBillingAddress->getCountryId(),
                'telephone' => $defaultBillingAddress->getTelephone()
            ]);
            $shippingInformation->setBillingAddress($billingAddress);
        }

        if ($order->getExtensionAttributes()->getShippingAssignments() !== null) {
            $shippingAssignments = $order->getExtensionAttributes()->getShippingAssignments();
            $shippingAddress = $this->addressFactory->create();
            $shippingAddress->setData([
                'firstname' => $shippingAssignments[0]->getShipping()->getAddress()->getFirstname(),
                'middlename' => $shippingAssignments[0]->getShipping()->getAddress()->getMiddlename(),
                'lastname' => $shippingAssignments[0]->getShipping()->getAddress()->getLastname(),
                'street' => $shippingAssignments[0]->getShipping()->getAddress()->getStreet(),
                'postcode' => $shippingAssignments[0]->getShipping()->getAddress()->getPostcode(),
                'city' => $shippingAssignments[0]->getShipping()->getAddress()->getCity(),
                'country_id' => $shippingAssignments[0]->getShipping()->getAddress()->getCountryId(),
                'telephone' => $shippingAssignments[0]->getShipping()->getAddress()->getTelephone()
            ]);
            $shippingInformation->setShippingAddress($shippingAddress);

            $shippingMethod = explode('_', $shippingAssignments[0]->getShipping()->getMethod());
            $shippingInformation->setShippingCarrierCode($shippingMethod[0]);
            $shippingInformation->setShippingMethodCode($shippingMethod[1]);
        } else {
            $customer = $this->customerRepository->getById($order->getCustomerId());
            $defaultShippingAddress = $this->addressRepository->getById($customer->getDefaultShipping());
            $shippingAddress = $this->addressFactory->create();
            $shippingAddress->setData([
                'firstname' => $defaultShippingAddress->getFirstname(),
                'middlename' => $defaultShippingAddress->getMiddlename(),
                'lastname' => $defaultShippingAddress->getLastname(),
                'street' => $defaultShippingAddress->getStreet(),
                'postcode' => $defaultShippingAddress->getPostcode(),
                'city' => $defaultShippingAddress->getCity(),
                'country_id' => $defaultShippingAddress->getCountryId(),
                'telephone' => $defaultShippingAddress->getTelephone()
            ]);
            $shippingInformation->setShippingAddress($shippingAddress);

            $shippingInformation->setShippingCarrierCode('flatrate');
            $shippingInformation->setShippingMethodCode('flatrate');
        }

        $this->shippingInformationManagement->saveAddressInformation($cartId, $shippingInformation);

        /** @var \Magento\Quote\Api\Data\PaymentInterface $paymentInformation */
        $paymentInformation = $this->paymentInterfaceFactory->create();
        $paymentInformation->setMethod($order->getPayment()->getMethod());
        $paymentInformation->setPoNumber($order->getCustomerNote());

        $orderId = $this->paymentInformationManagement->savePaymentInformationAndPlaceOrder($cartId, $paymentInformation);

        $createdOrder = $this->orderRepository->get($orderId);

        /**
         * Set correct totals for offline order
         */
        $createdOrder->setBaseSubtotal($order->getBaseSubtotal());
        $createdOrder->setSubtotal($order->getSubtotal());
        $createdOrder->setBaseSubtotalInclTax($order->getBaseSubtotalInclTax());
        $createdOrder->setSubtotalInclTax($order->getSubtotalInclTax());
        $createdOrder->setBaseTaxAmount($order->getBaseTaxAmount());
        $createdOrder->setTaxAmount($order->getTaxAmount());
        $createdOrder->setBaseShippingAmount($order->getBaseShippingAmount());
        $createdOrder->setShippingAmount($order->getShippingAmount());
        $createdOrder->setBaseGrandTotal($order->getBaseGrandTotal());
        $createdOrder->setGrandTotal($order->getGrandTotal());

        $createdOrder->setDiscountAmount(-$order->getDiscountAmount());
        $createdOrder->setBaseDiscountAmount(-$order->getBaseDiscountAmount());
        $createdOrder->setCouponCode($order->getCouponCode());
        $createdOrder->setDiscountDescription($order->getCouponCode());

        $createdOrder->setCreatedAt($order->getCreatedAt());
        $createdOrder->setCustomerNote($order->getCustomerNote());

        /** @var \Magento\Sales\Model\Order\Item $item */
        foreach ($createdOrder->getItems() as $item) {
            foreach ($savedItems as $savedItem) {
                if ($savedItem['cartItem']->getId() == $item->getQuoteItemId()) {
                    $item->setBaseTaxAmount($savedItem['orderItem']->getBaseTaxAmount());
                    $item->setTaxAmount($savedItem['orderItem']->getTaxAmount());
                    $item->setTaxPercent($savedItem['orderItem']->getTaxPercent());

                    $item->setBaseOriginalPrice($savedItem['orderItem']->getBaseOriginalPrice());
                    $item->setOriginalPrice($savedItem['orderItem']->getOriginalPrice());

                    $item->setBasePrice($savedItem['orderItem']->getBasePrice());
                    $item->setPrice($savedItem['orderItem']->getPrice());

                    $item->setBasePriceInclTax($savedItem['orderItem']->getBasePriceInclTax());
                    $item->setPriceInclTax($savedItem['orderItem']->getPriceInclTax());

                    $item->setBaseDiscountAmount($savedItem['orderItem']->getBaseDiscountAmount());
                    $item->setDiscountAmount($savedItem['orderItem']->getDiscountAmount());

                    $item->setBaseRowTotal($savedItem['orderItem']->getBaseRowTotal());
                    $item->setRowTotal($savedItem['orderItem']->getRowTotal());

                    $item->setBaseRowTotalInclTax($savedItem['orderItem']->getBaseRowTotalInclTax());
                    $item->setRowTotalInclTax($savedItem['orderItem']->getRowTotalInclTax());

                    try {
                        $this->orderItemRepository->save($item);
                    } catch (\Exception $ex) {
                        try {
                            $this->orderRepository->delete($createdOrder);
                        } catch (\Exception $removalException) {
                            throw new \Magento\Framework\Webapi\Exception(new \Magento\Framework\Phrase('Item Totals Update Failed + Order removal failed: ' . __($item->getSku() . ' - ' . $ex->getMessage()) . '. Order error: ' . $removalException->getMessage()), $ex->getCode());
                        }

                        throw new \Magento\Framework\Webapi\Exception(new \Magento\Framework\Phrase('Item Totals Update Failed: ' . __($item->getSku() . ' - ' . $ex->getMessage())), $ex->getCode());
                    }
                }
            }
        }

        /**
         * Cancel old order if present
         */
        if ($order->getEntityId() !== null) {
            $baseOrder = $this->orderRepository->get($order->getEntityId());

            $originalId = $baseOrder->getOriginalIncrementId();
            if (!$originalId) {
                $originalId = $baseOrder->getIncrementId();
            }

            $createdOrder->setOriginalIncrementId($originalId);
            $createdOrder->setRelationParentId($baseOrder->getEntityId());
            $createdOrder->setRelationParentRealId($baseOrder->getIncrementId());
            $createdOrder->setEditIncrement($baseOrder->getEditIncrement() + 1);
            $createdOrder->setIncrementId($originalId . '-' . ($baseOrder->getEditIncrement() + 1));

            $baseOrder->setRelationChildId($createdOrder->getEntityId());
            $baseOrder->setRelationChildRealId($createdOrder->getIncrementId());

            try {
                $this->orderRepository->save($baseOrder);
            } catch (\Exception $ex) {
                try {
                    $this->orderRepository->delete($createdOrder);
                } catch (\Exception $removalException) {
                    throw new \Magento\Framework\Webapi\Exception(new \Magento\Framework\Phrase('Unable to update base order + order removal failed: ' . __($baseOrder->getEntityId() . ' - ' . $ex->getMessage()) . '. Order error: ' . $removalException->getMessage()), $ex->getCode());
                }

                throw new \Magento\Framework\Webapi\Exception(new \Magento\Framework\Phrase('Unable to update base order: ' . __($baseOrder->getEntityId() . ' - ' . $ex->getMessage())), $ex->getCode());
            }

            try {
                $this->orderManagement->cancel($order->getEntityId());
            } catch (\Exception $ex) {
                try {
                    $this->orderRepository->delete($createdOrder);
                } catch (\Exception $removalException) {
                    throw new \Magento\Framework\Webapi\Exception(new \Magento\Framework\Phrase('Unable to cancel base order + order removal failed: ' . __($baseOrder->getEntityId() . ' - ' . $ex->getMessage()) . '. Order error: ' . $removalException->getMessage()), $ex->getCode());
                }

                throw new \Magento\Framework\Webapi\Exception(new \Magento\Framework\Phrase('Unable to cancel base order: ' . __($baseOrder->getEntityId() . ' - ' . $ex->getMessage())), $ex->getCode());
            }
        }

        $createdOrder->setExtensionAttributes($order->getExtensionAttributes());
        if (!empty($order->getStatus())) {
            $createdOrder->setStatus($order->getStatus());
        }

        try {
            $createdOrder = $this->orderRepository->save($createdOrder);
        } catch (\Exception $ex) {
            $entityId = $createdOrder->getEntityId();

            try {
                $this->orderRepository->delete($createdOrder);
            } catch (\Exception $removalException) {
                throw new \Magento\Framework\Webapi\Exception(new \Magento\Framework\Phrase('Unable to update order + order removal failed: ' . __($entityId . ' - ' . $ex->getMessage()) . '. Order error: ' . $removalException->getMessage()), $ex->getCode());
            }

            throw new \Magento\Framework\Webapi\Exception(new \Magento\Framework\Phrase('Unable to update order: ' . __($entityId . ' - ' . $ex->getMessage())), $ex->getCode());
        }

        $this->orderSender->send($createdOrder);

        return $createdOrder;
    }
}