<?php

namespace Conneqt\Base\Model;

class BundledOption implements \Conneqt\Base\Api\Data\BundledOptionInterface
{
    /** @var \Conneqt\Base\Api\Data\BundledOptionSelectionInterface[]|null */
    protected $selections = null;

    /** @var int */
    protected $qty;

    /**
     * @return \Conneqt\Base\Api\Data\BundledOptionSelectionInterface[]
     */
    public function getSelections()
    {
        return $this->selections;
    }

    /**
     * @param $selections \Conneqt\Base\Api\Data\BundledOptionSelectionInterface[]
     * @return \Conneqt\Base\Api\Data\BundledOptionInterface
     */
    public function setSelections($selections)
    {
        $this->selections = $selections;

        return $this;
    }

    /**
     * @param $qty int
     * @return \Conneqt\Base\Api\Data\BundledOptionSelectionInterface
     */
    public function setQty($qty)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }
}