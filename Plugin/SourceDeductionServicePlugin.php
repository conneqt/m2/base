<?php

namespace Conneqt\Base\Plugin;

class SourceDeductionServicePlugin
{
    public function __construct(
        private \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
    }

    public function aroundExecute($subject, callable $proceed, $sourceDeductionRequest)
    {
        if ($this->isEnabled()) {
            return;
        }

        $proceed($sourceDeductionRequest);
    }


    private function isEnabled(): bool
    {
        return
            $this->scopeConfig->getValue('conneqt_base/settings/enable_shipping_when_no_stock') === '1';
    }
}