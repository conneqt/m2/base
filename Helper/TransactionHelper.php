<?php
/**
 * Created by PhpStorm.
 * User: jeroen
 * Date: 16-10-17
 * Time: 20:32
 */

namespace Conneqt\Base\Helper;

class TransactionHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_transactionResource;
    protected $_transactionFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Conneqt\Base\Model\ResourceModel\Transaction $transactionResource,
        \Conneqt\Base\Model\TransactionFactory $transactionFactory)
    {
        parent::__construct($context);

        $this->_transactionResource = $transactionResource;
        $this->_transactionFactory = $transactionFactory;
    }

    public function addTransaction($entityType, $entityId)
    {
        $transaction = $this->_transactionFactory->create();

        $transaction
            ->setEntityType($entityType)
            ->setEntityIdentifier($entityId);

        $this->_transactionResource->save($transaction);
    }
}